import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { Link } from 'react-router-dom';
export function HomePage() {
  return (
    <>
      <Helmet>
        <title>Login Page</title>
        <meta name="description" content="To-Do application Login page" />
      </Helmet>
      <div>
        <form>
          <div className="Apl">
            Username: <input type="text" id="uname" />
          </div>
          <br />
          <br />
          <div className="Aps">
            <Link to="dashboard">
              <button id="submit" className="Aps">
                Submit
              </button>
            </Link>
          </div>
        </form>
      </div>
    </>
  );
}
