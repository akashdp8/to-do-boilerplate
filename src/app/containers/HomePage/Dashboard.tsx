/* eslint-disable prettier/prettier */
import React from "react";
import {connect} from 'react-redux';

function Dashboard() {
  return <h1>To Do List</h1>;
}

function mapStateToProps(state){
    return{
        tasks: state.tasks
    }
}

export const ConnectedDashboard = connect(mapStateToProps)(Dashboard)
export default Dashboard;