import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState } from './types';

// The initial state of the Form container
export const initialState: ContainerState = {};

const formSlice = createSlice({
  name: 'form',
  initialState,
  reducers: {
    someAction(state, action: PayloadAction<any>) {},
  },
});

export const { actions: formActions, reducer, name: sliceKey } = formSlice;
