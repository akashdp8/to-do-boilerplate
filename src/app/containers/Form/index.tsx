/**
 *
 * Form
 *
 */

import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components/macro';

import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { reducer, sliceKey } from './slice';
import { selectForm } from './selectors';
import { formSaga } from './saga';

interface Props {}

export function Form(props: Props) {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: formSaga });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const form = useSelector(selectForm);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const dispatch = useDispatch();

  return (
    <>
      <Helmet>
        <title>Form</title>
        <meta name="description" content="Description of Form" />
      </Helmet>
      <Div></Div>
    </>
  );
}

const Div = styled.div``;
