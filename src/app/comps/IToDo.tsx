/* eslint-disable prettier/prettier */
// import React from 'react';

interface IToDo {
  owner: string,
  name: string;
  status: boolean;
}

export default IToDo;