/* eslint-disable prettier/prettier */
import React from 'react';
// import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
// import './style.css';

const Page2 = () => {
  return (
    <div className="Pag2">
      <Dropdown>
        <Dropdown.Toggle variant="success" id="dropdown-basic">
          Completed Tasks
        </Dropdown.Toggle>

        <Dropdown.Menu>
          <Dropdown.Item className="menu">Task 1</Dropdown.Item>
          <br />
          <Dropdown.Item className="menu">Task 2</Dropdown.Item>
          <br />
          <Dropdown.Item className="menu">Task 3</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
      <div className="box">
        <input type="checkbox"></input>
        <p>Task 1</p>
        <input type="checkbox"></input>
        <p>Task 2</p>
        <input type="checkbox"></input>
        <p>Task 3</p>
      </div>
    </div>
  );
};

export default Page2;
