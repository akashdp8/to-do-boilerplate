/* eslint-disable prettier/prettier */
import { createStore } from 'redux';
import defaultState from '../../server/defaultState';

const store = createStore(function reducer(state = defaultState, action) {
  return state;
});

export default store;