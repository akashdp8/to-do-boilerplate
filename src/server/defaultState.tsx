/* eslint-disable prettier/prettier */
const defaultState = {
  users: [
    {
      id: 'U1',
      name: 'Dev',
    },
  ],
  tasks: [{
      task: "Task 1",
      owner: "U1"
  }]
};

export default defaultState;